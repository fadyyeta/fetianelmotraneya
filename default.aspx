﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="FetianElMotraneya._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	
    <!-- Basic Page Needs -->
	<meta charset="UTF-8">

    	<!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; minimum-scale=1.0; user-scalable=no; target-densityDpi=device-dpi" />
    
	
            	<!-- Favicons -->
        <link href="wp-content/themes/bizpro/images/favicon.png" rel="shortcut icon" /> 
        	    
   
    <title>El Motraneya | E3dady ElMotraneya BNS</title>
		
	<link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
<link rel='dns-prefetch' href='http://s.w.org/' />
<link rel="alternate" type="application/rss+xml" title="Biz Pro &raquo; Feed" href="feed/index.html" />
<link rel="alternate" type="application/rss+xml" title="Biz Pro &raquo; Comments Feed" href="comments/feed/index.html" />
    
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/demo.themetor.com\/bizpro\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.6.11"}};
			!function(a,b,c){function d(a){var c,d,e,f,g,h=b.createElement("canvas"),i=h.getContext&&h.getContext("2d"),j=String.fromCharCode;if(!i||!i.fillText)return!1;switch(i.textBaseline="top",i.font="600 32px Arial",a){case"flag":return i.fillText(j(55356,56806,55356,56826),0,0),!(h.toDataURL().length<3e3)&&(i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,65039,8205,55356,57096),0,0),c=h.toDataURL(),i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,55356,57096),0,0),d=h.toDataURL(),c!==d);case"diversity":return i.fillText(j(55356,57221),0,0),e=i.getImageData(16,16,1,1).data,f=e[0]+","+e[1]+","+e[2]+","+e[3],i.fillText(j(55356,57221,55356,57343),0,0),e=i.getImageData(16,16,1,1).data,g=e[0]+","+e[1]+","+e[2]+","+e[3],f!==g;case"simple":return i.fillText(j(55357,56835),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode8":return i.fillText(j(55356,57135),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode9":return i.fillText(j(55358,56631),0,0),0!==i.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity","unicode9"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
    <style>
        #firstLoader {
            margin-top: 25% !important;
        }
    </style>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='stylesheet'  href='wp-content/themes/bizpro/style5152.css?ver=1.0' type='text/css' media='all' />
<style id='stylesheet_inline' type='text/css'>
body{ font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:12px;font-weight: normal;color: #7A7A7A;background-color:#FFFFFF !important;}::selection{background:#099aad !important;}::-moz-selection{background:#099aad !important;}h1{ font-family: Ubuntu, Arial, Helvetica, sans-serif; font-size: 36px; font-weight: bold; color: #4a4a4a; }h2{ font-family: Ubuntu, Arial, Helvetica, sans-serif; font-size: 28px; font-weight: bold; color: #4a4a4a; }h3{ font-family: Ubuntu, Arial, Helvetica, sans-serif; font-size: 20px; font-weight: bold; color: #4a4a4a; }h4{ font-family: Ubuntu, Arial, Helvetica, sans-serif; font-size: 16px; font-weight: bold; color: #4a4a4a; }h5,.testimonial-s strong{ font-family: Ubuntu, Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; color: #4a4a4a; }h6{ font-family: Ubuntu, Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #4a4a4a; }h1 a, h2 a, h3 a, h4 a, h5 a, h6 a, h1 a:visited, h2 a:visited, h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited  { font-weight: inherit; color: inherit; }a{ color: #4a4a4a; }a:hover, a:focus{ color: #272727; }footer a:hover, footer a:focus{ color: #ffffff; }#layout,.liteAccordion.flat,.liteAccordion.flat .slide > div{background-color:#ffffff}.sf-menu{padding-top:40px}.sf-menu a{ font-size: 16px;color: #444444;font-family: Ubuntu, Arial, Helvetica, sans-serif;font-weight: bold;text-transform:capitalize;}.sf-menu li a:hover, .sf-menu > li:hover > a,.sf-menu > li > a:hover, li.current-menu-item > a, .sf-menu ul li:hover > a, .sf-menu a.current-menu-item i, .sf-menu a:hover i,.thead .info_bar a:hover{ color: #099aad;}.sf-menu li.current-menu-item > a:before,.sf-menu li.current-menu-item > a:after,.search_fit_cart{ border-color: #099aad;}.head{color:#777777;background-color:#FFFFFF;background-color:rgba(255,255,255,0.6);}.head.sticky{background-color:#FFFFFF}.thead .info_bar,.thead .info_bar a{color:#444444}.thead .info_bar i{color:#099aad}.head .info_bar{font-size:16px}.social a,.social_widget i{color:#444444}.search_icon i{color:#099aad}.T20_x,.T20_x:before, .T20_x:after{background-color:#099aad}figure .hover_icons a:hover,.woocommerce ul.products li.product .button:hover,.liteAccordion.light .slide > h2.selected span, .liteAccordion.light .slide h2.selected span:hover,.splitter ul li.current a, .filter_masonry ul li.current a,.liteAccordion.flat .slide > h2.selected span, .liteAccordion.flat .slide h2.selected span:hover,.liteAccordion.flat .ap-caption,.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt	{background-color:#099aad !important;}.tipsy-n .tipsy-arrow,.tipsy-e .tipsy-arrow,.tipsy-w .tipsy-arrow {border-bottom-color:#099aad !important}.tipsy-s .tipsy-arrow{border-top-color:#099aad !important}.thead{background-color:#ffffff !important;background-color:rgba(255,255,255,0.7) !important;}.sf-menu li ul{background-color:#ffffff !important;}.sf-menu li li a{color:#444444 !important; font-size:12px}.sf-menu i{color:#099aad !important}.hasScrolled .logo{margin:18px 0px 0px;width:130px}.logo{width:170px;margin-top:40px;margin-bottom:40px}.blog_post .post_title:after, .title_line:after,.mega h4:after, .widget h4:after,.title_line:before{color:#099aad !important;}.widget li a:before,	.countdown li span,	.accordion-head p:hover,.toggle-head a:hover,.wpb_toggle_title a:hover,.ui-accordion-header a:hover,.detailes h5 a,.owl-prev:hover i,.owl-next:hover i,.dark .Bhead .table-badge,.splitter.st4 ul li.current a, .filter_masonry.st4 ul li.current a,	.splitter.st5 ul li.current a, .filter_masonry.st5 ul li.current a,	.splitter.st6 ul li.current a, .filter_masonry.st6 ul li.current a,	.splitter.st7 ul li.current a, .filter_masonry.st7 ul li.current a,.pricing-table .head h2{color: #099aad !important;}.widget_product_search input[type="text"]:focus,.woocommerce table.cart td.actions .coupon .input-text:focus,.tags a:hover,.tagcloud a:hover,ul.cart_list li img:hover,ul.product_list_widget li img:hover,.splitter.st5 ul li.current a, .filter_masonry.st5 ul li.current a,.splitter.st6 ul li.current a, .filter_masonry.st6 ul li.current a,.splitter.st7 ul li.current a, .filter_masonry.st7 ul li.current a,.recent_list_blog h3:after{border-color:#099aad !important;}.liteAccordion.flat .slide > h2 span{background-color: #4a4a4a; }i,.tags a:before, .widget_tag_cloud a:before, .widget_bizpro_tags a:before, .wp-tag-cloud li a:before, .tagcloud a:before{color:#099aad;}#toTop:hover i,footer i,.close_shop i,.close_search i,#popup .search_place i{color:#FFFFFF;}.social.with_color i{color:#FFF !important;}.btn.tbutton5.default,.btn.tbutton6.default,.btn.tbutton7.default,.btn.tbutton8.default,.filterable.st4 ul.filter li.current,.projectslider .flex-direction-nav a:hover,.pagination-tt ul li span{background-color:#099aad;}.btn.tbutton1.default,.btn.tbutton2.default,.btn.tbutton3.default,.btn.tbutton4.default,.filterable.st6 ul.filter li.current a,.filterable.st7 ul.filter li.current a,.projectslider .flex-direction-nav a:hover,.pagination-tt ul li span {border-color:#099aad;color:#099aad;}footer {color:#666666;}footer a{color:#666666}footer a:hover{color:#ffffff}footer .sub_footer .copyright{color:#ffffff}footer .sub_footer .copyright a{color:#ffffff !important;}footer .sub_footer .copyright a:hover{color:#ffffff !important;}footer i{color:#a5a5a5 !important;}footer .widget > h4:after{color:#099aad}footer .widget > h4{color:#099aad}.foot-menu li a{color:#ffffff}.foot-menu li a:hover{color:#ffffff}footer{background-color:#ffffff !important;}footer{background-color:#ffffff !important;}.sub_footer{background-color:#099aad !important;}.breadcrumbs{background-color:#eeeeee;padding:30px 0px;}.page-title{color:#444444;}.breadcrumbIn span,.breadcrumbIn ul,.breadcrumbIn ul li,.breadcrumbIn ul li a{color:#666666;}.thead,.kwickslider.kwicks_border ul li,.widget, .blog_post,#newsletter input, .widget #searchwidget input, .coupon_input,.blog_post.format-standard img, .owl-carousel .owl-wrapper-outer, .iframe-thumb,.blog_post .cats,.btn,footer,.instafeed a img,.owl-theme .owl-controls .owl-buttons div,.w_border a,.sub_footer,#toTop,
	.full .big-slider,.big-slider h3,.big-slider p,.big-slider .flex-direction-nav a,.tagcloud a,
	.active .accordion-head, .active .toggle-head,.active .accordion-content, .active .toggle-content,
	.accordion-head, .toggle-head, .wpb_toggle, .wpb_accordion_header,.wpb_accordion_section .ui-widget-content,
	.action,.logo_carousel img,.countdown li,.member .member-social,.member,.breaking-news-bar,.pricing-table,
	.grid figure,.blog_b,.tabs li a.active, .wpb_tabs_nav li.ui-tabs-active,.tabs-content, .wpb_tab,
	.bizpro_testimonials.light,.light .custom_block2 li img,.item_det,#commentform textarea,.head .social a,header.header_v11 .head,header.header_v12 .head,
	#commentform input,.with_details li,.pagination-tt ul li a:hover, blockquote, .blockquote,
	.woocommerce .shop-links a,.wpcf7 input, .wpcf7 textarea, .wpcf7 select,.nav-arrows a,.liteAccordion.flat,.roundabout li,
	.woo-title .filter_wrapper .woocommerce-ordering, .pa_selection,.widget_product_search input[type="text"],.widget select,.post-password-form input,ul.comment-list .comment-body,ul.comment-list .reply,.comment-author img,.bizpro_flickr .flickr_badge_image,
	.portfolio-sidebar .portfolio-meta,figure.portfolio-single img,.portfolio_detail_content .portfolio-meta,.woocommerce nav.woocommerce-pagination ul li,
.blog_b .post_tm,ul.tabs li:first-child a.active:before ,ul.wpb_tabs_nav li.ui-tabs-active:first-child:before,
.owl-carousel .owl-controls .owl-nav .owl-prev,.owl-carousel .owl-controls .owl-nav .owl-next,
.search_post,.process_box.horizontal_process,.process_box.horizontal_process span,.process_box.vertical_process,.process_box.vertical_process span{
	border-color:#cccccc !important;}.kwickslider,.kwickslider ul{height:317px;}.kwickslider ul li{height:315px;}.shop_iconZZZ {top:45px;}
		.shop_icon i{color:#099aad;}
		.shop_icon b{color:#ffffff;background-color:#888888 !important;}
		.woocommerce .widget_price_filter .ui-slider .ui-slider-range,
		.woocommerce .ui-widget-header,
		.woocommerce .widget_layered_nav_filters ul li a,
		.woocommerce .ui-slider .ui-slider-handle{background-color:#099aad !important}
		.woocommerce #content input.button,
		.woocommerce #respond input#submit,
		.woocommerce button.button,
		.woocommerce input.button,
		.woocommerce #content .quantity .minus,
		.woocommerce #content .quantity .plus,
		.woocommerce .quantity .minus,
		.woocommerce .quantity .plus,
		.woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover{background:#099aad !important;}
		.woocommerce a span.onsale, .woocommerce span.onsale{background:#099aad !important;}.tparrows{
	   background:none !important;
	   background-color:transparent !important;
	   border:none;
	   opacity:.6;
	   -webkit-transition: all 0.3s ease;
		transition: all 0.3s ease;
	   }
	.tparrows:before {
	   text-align:center;
	   font-family:Fontawesome !important;
	   line-height:40px;
	   font-size:60px !important;
	   color:#099aad !important;
	   border-radius:0;
	   background:none !important;
	   background-color:transparent !important;
	   border:none;
	   opacity:.6;
	   -webkit-transition: all 0.3s ease;
		transition: all 0.3s ease;
	   }
	 .hebe.tparrows:before {font-size:50px !important;}
	.tp-leftarrow:before {content: "\f104" !important;}
	.tp-rightarrow:before	 {content: "\f105" !important;}
	.tp-rightarrow:hover:before,.tp-leftarrow:hover:before{opacity:1;font-size:70px}
	.tp-bullets{background:none !important;}
	.rev_slider_wrapper .tp-bullets .tp-bullet{
		background:#aaa;
		width:10px !important;
		height:10px !important;
		margin:0 10px !important;
		border:none;
		border-radius:50%;
		opacity:.4;
		-webkit-transition: all 0.3s ease;
		transition: all 0.3s ease;
		}
	.rev_slider_wrapper .tp-bullets .tp-bullet:hover{transform:scale(1.2);opacity:1;}
	.rev_slider_wrapper .tp-bullets .tp-bullet.selected{opacity:1;background-color:#099aad !important;}
</style>
<link rel='stylesheet' id='fontawesome_css'  href='wp-content/themes/bizpro/css/font-awesome/css/font-awesome.min5152.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='bizpro_flexslider_css'  href='wp-content/themes/bizpro/css/flex-slider5152.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='bizpro_wooshop_css'  href='wp-content/themes/bizpro/css/shop5152.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='bizpro_flaticon_css'  href='wp-content/themes/bizpro/css/flaticon/flaticon5152.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='animate_css'  href='wp-content/themes/bizpro/css/animate.min5152.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='bizpro_responsive_css'  href='wp-content/themes/bizpro/css/responsive5152.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='contact_form_7_css'  href='wp-content/plugins/contact-form-7/includes/css/styles8686.css?ver=4.5.1' type='text/css' media='all' />
<link rel='stylesheet' id='rs_plugin_settings_css'  href='wp-content/plugins/revslider/public/assets/css/settings5223.css?ver=5.2.6' type='text/css' media='all' />
<style id='rs-plugin-settings-inline_css' type='text/css'>
#rs-demo-id {}
</style>
<link rel='stylesheet' id='woocommerce_layout_css'  href='wp-content/plugins/woocommerce/assets/css/woocommerce-layout72e6.css?ver=2.6.4' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce_smallscreen_css'  href='wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen72e6.css?ver=2.6.4' type='text/css' media='only screen and (max-width: 768px)' />
<link rel='stylesheet' id='woocommerce_general_css'  href='wp-content/plugins/woocommerce/assets/css/woocommerce72e6.css?ver=2.6.4' type='text/css' media='all' />
<link rel='stylesheet' id='bizpro_custom_google_fonts_css'  href='http://fonts.googleapis.com/css?family=Ubuntu%3A400%2C400italic%2C700%2C700italic%7CUbuntu%3A400%2C400italic%2C700%2C700italic%7CUbuntu%3A400%2C400italic%2C700%2C700italic%7CUbuntu%3A400%2C400italic%2C700%2C700italic%7CUbuntu%3A400%2C400italic%2C700%2C700italic%7CUbuntu%3A400%2C400italic%2C700%2C700italic%7CUbuntu%3A400%2C400italic%2C700%2C700italic%7CUbuntu%3A400%2C400italic%2C700%2C700italic%7C&amp;ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_front_css'  href='wp-content/plugins/js_composer/assets/css/js_composer.min9d08.css?ver=4.12.1' type='text/css' media='all' />
<script type='text/javascript' src='wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
<script type='text/javascript' src='wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1'></script>
<script type='text/javascript' src='wp-content/themes/bizpro/js/bizpro.min5152.js?ver=1.0'></script>
<script type='text/javascript' src='wp-content/themes/bizpro/js/jquery.prettyPhotoa07c.js?ver=4.6.11'></script>
<script type='text/javascript' src='wp-content/themes/bizpro/js/owl.carousel.mina07c.js?ver=4.6.11'></script>
<script type='text/javascript' src='wp-content/themes/bizpro/js/custom5152.js?ver=1.0'></script>
<script type='text/javascript'>
function bizpro_lightbox(){var lbarray = {hook:'data-gal',animation_speed: 'fast',overlay_gallery:true,autoplay_slideshow:false,slideshow:5000,theme: 'light_rounded',opacity: 0.8,show_title:false,social_tools: "",allow_resize: true,counter_separator_label: 'of',deeplinking: false,	default_width: 900,default_height: 500};var slctr='a[data-gal^="lightbox"]';
		jQuery(slctr).prettyPhoto(lbarray);var windowWidth =window.screen.width < window.outerWidth ? window.screen.width : window.outerWidth;
        var issmall = windowWidth < 500;if(issmall){jQuery(slctr).unbind('click.prettyphoto');}}bizpro_lightbox();
</script>
<script type='text/javascript' src='wp-content/themes/bizpro/js/jquery.flexslider-min53cf.js?ver=2.1'></script>
<script type='text/javascript' src='wp-content/themes/bizpro/js/instafeed.min3ba1.js?ver=1.3.3'></script>
<script type='text/javascript' src='wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min5223.js?ver=5.2.6'></script>
<script type='text/javascript' src='wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min5223.js?ver=5.2.6'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/bizpro\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/bizpro\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View Cart","cart_url":"http:\/\/demo.themetor.com\/bizpro\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min72e6.js?ver=2.6.4'></script>
<script type='text/javascript' src='wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart9d08.js?ver=4.12.1'></script>
<link rel='https://api.w.org/' href='wp-json/index.html' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.6.11" />
<meta name="generator" content="WooCommerce 2.6.4" />
<link rel="canonical" href="index.html" />
<link rel='shortlink' href='index.html' />
<link rel="alternate" type="application/json+oembed" href="wp-json/oembed/1.0/embedc923.json?url=http%3A%2F%2Fdemo.themetor.com%2Fbizpro%2F" />
<link rel="alternate" type="text/xml+oembed" href="wp-json/oembed/1.0/embed34a1?url=http%3A%2F%2Fdemo.themetor.com%2Fbizpro%2F&amp;format=xml" />

		                <meta name="description" content="موقع خدمة اعدادى بمطرانية بنى سويف" /> 
                
        
                        
        <meta name="robots" content="index, follow" />
<meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://demo.themetor.com/bizpro/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="http://demo.themetor.com/bizpro/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution 5.2.6 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<style type="text/css" data-type="vc_shortcodes-custom_css">.vc_custom_1473104552943{margin-top: -115px !important;}.vc_custom_1473022964624{background-color: #099aad !important;}.vc_custom_1473170988004{background-color: #099aad !important;}.vc_custom_1474329284117{background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
    
</head>
<body class="home page page-id-4 page-template-default wpb-js-composer js-comp-ver-4.12.1 vc_responsive">
    <form  runat="server">
	    
	
	<script>
    	var surl='wp-content/themes/bizpro/customizer/index.html';
		var sturl='wp-content/themes/bizpro/index.html';
    </script>



		<div id="layout" class="full">    
    
<header class="header_v6 header_v7  overlay-header">


    <div class="head">
        <div class="row clearfix">
            
 			<div class="logo">
                <a href="index.html"><img src="wp-content/themes/bizpro/images/logo.png" alt="Biz Pro" /></a>
            </div><!-- end logo -->
            
            
            <div class="search_fit_cart">
                        
            <div class="search_icon"><i class="fa fa-search"></i></div><!-- /search icon-->

            <!-- search popup-->
            <div class="search">
                <div id="popup">
                    <div class="search_place">
                        <div class="s_form">
                            <form id="search" action="http://demo.themetor.com/bizpro/" class="searchform" method="get">
                                <input id="inputhead" name="s" type="text" placeholder="Search...">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form><!-- /form -->
                        </div><!-- /form -->
                    </div><!-- /search place -->

                    <div class="close_search"><i class="fa fa-remove"></i></div>
                </div><!-- /popup -->
                <div id="SearchBackgroundPopup"></div>
            </div>
            
            <!-- /search popup-->
            
            
<div class="shopping_bag"><div class="shop_icon"></div></div>
			
            </div><!-- /search_fit_cart-->
            
             
                            <div class="info_bar">
                    <i class="fa fa-envelope"></i> <a href="mailto:info@yourname.com">info@yourname.com</a><i class="fa fa-phone"></i> +1 (888) 0000                </div><!-- end info -->
            
            
            
        </div><!-- row -->
    </div><!-- headdown -->
    
    
    <div class="thead  my_sticky smart_sticky">
        <div class="row clearfix">
        	
			                <div class="social w_radius">			
                    

		
    
    
        
        
            <a class="fa ico-twitter fa-twitter toptip" href="#"  target="_blank"  title="Twitter"></a >	
     
      
        
            <a class="fa ico-facebook fa-facebook toptip" href="#"  target="_blank"  title="Facebook"></a >	
     
    

     
    
    
     
    
            <a class="fa ico-flickr fa-flickr toptip" href="#"  target="_blank"  title="Flickr"></a >	
     
    
    
     
    
    
     
    
    
     
    
    
     
    
    
     
    
    
     
    
     

    
     
    
    
            <a class="fa ico-rss fa-rss toptip" href="#"  target="_blank"  title="RSS"></a >	
     
    
    
     
    
                </div><!-- end social -->         
                         
            

            <nav id="mobile_m" class="main"><ul id="menu-main-menu" class="sf-menu"><li id="menu-item-1217" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4 current_page_item menu-item-has-children"><a href="index.html">Home<span class="subtitle"></span></a>
<ul class="sub-menu">
	<li id="menu-item-1224" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="one-page/index.html">One Page (Landing Page)<span class="subtitle"></span></a></li>
	<li id="menu-item-1223" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="nivoslider/index.html">Nivoslider<span class="subtitle"></span></a></li>
	<li id="menu-item-1222" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="flex-slider/index.html">Flex Slider<span class="subtitle"></span></a></li>
	<li id="menu-item-1221" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="3d-slider/index.html">3D Slider<span class="subtitle"></span></a></li>
	<li id="menu-item-1220" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="kwicks-slider/index.html">Kwicks Slider<span class="subtitle"></span></a></li>
	<li id="menu-item-1218" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="liteaccordion-slider/index.html">Liteaccordion Slider<span class="subtitle"></span></a></li>
	<li id="menu-item-1219" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="roundabout-slider/index.html">Roundabout Slider<span class="subtitle"></span></a></li>
</ul>
</li>
<li id="menu-item-20" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="about-us/index.html">About Us<span class="subtitle"></span></a></li>
<li id="menu-item-17" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="services/index.html">Services<span class="subtitle"></span></a></li>
<li id="menu-item-16" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children"><a href="portfolio/index.html">Projects<span class="subtitle"></span></a>
<ul class="sub-menu">
	<li id="menu-item-236" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children"><a href="#">Portfolio Grid Style<span class="subtitle"></span></a>
	<ul class="sub-menu">
		<li id="menu-item-669" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="portfolio-1-column/index.html">Portfolio 1 Column<span class="subtitle"></span></a></li>
		<li id="menu-item-668" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="portfolio-2-columns/index.html">Portfolio 2 Columns<span class="subtitle"></span></a></li>
		<li id="menu-item-661" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="portfolio-2-columns-details/index.html">2 Columns + Details<span class="subtitle"></span></a></li>
		<li id="menu-item-667" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="portfolio-3-columns/index.html">3 Columns<span class="subtitle"></span></a></li>
		<li id="menu-item-724" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="portfolio-3-columns-style2/index.html">3 Columns (Style2)<span class="subtitle"></span></a></li>
		<li id="menu-item-666" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="portfolio-4-columns/index.html">4 Columns<span class="subtitle"></span></a></li>
		<li id="menu-item-723" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="portfolio-4-columns-style2/index.html">4 Columns (Style2)<span class="subtitle"></span></a></li>
	</ul>
</li>
	<li id="menu-item-237" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children"><a href="#">Portfolio Masonry Style<span class="subtitle"></span></a>
	<ul class="sub-menu">
		<li id="menu-item-665" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="portfolio-2-columns-masonry/index.html">2 Columns<span class="subtitle"></span></a></li>
		<li id="menu-item-664" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="portfolio-3-columns-masonry/index.html">3 Columns<span class="subtitle"></span></a></li>
		<li id="menu-item-663" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="portfolio-4-columns-masonry/index.html">4 Columns<span class="subtitle"></span></a></li>
		<li id="menu-item-662" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="portfolio-5-columns-masonry/index.html">5 Columns<span class="subtitle"></span></a></li>
	</ul>
</li>
</ul>
</li>
<li id="menu-item-585" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children"><a href="blog/index.html">Blog<span class="subtitle"></span></a>
<ul class="sub-menu">
	<li id="menu-item-625" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="blog/index.html">Blog Large Image<span class="subtitle"></span></a></li>
	<li id="menu-item-624" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="blog-medium-image/index.html">Blog Medium Image<span class="subtitle"></span></a></li>
	<li id="menu-item-623" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="blog-small-image/index.html">Blog Small Image<span class="subtitle"></span></a></li>
	<li id="menu-item-622" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="blog-both-sidebars/index.html">Blog Both Sidebars<span class="subtitle"></span></a></li>
	<li id="menu-item-621" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="blog-both-sidebars-right/index.html">Blog Both Sidebars Right<span class="subtitle"></span></a></li>
	<li id="menu-item-620" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="blog-both-sidebars-left/index.html">Blog Both Sidebars Left<span class="subtitle"></span></a></li>
	<li id="menu-item-616" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="blog-full-wide/index.html">Blog Full Wide<span class="subtitle"></span></a></li>
	<li id="menu-item-619" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="blog-1-column-eo/index.html">Blog 1 Column e/o<span class="subtitle"></span></a></li>
	<li id="menu-item-618" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="blog-2-columns-masonry/index.html">Blog 2 Columns Masonry<span class="subtitle"></span></a></li>
	<li id="menu-item-617" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="blog-3-columns-masonry/index.html">Blog 3 Columns Masonry<span class="subtitle"></span></a></li>
</ul>
</li>
<li id="menu-item-18" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="faq/index.html">FAQ<span class="subtitle"></span></a></li>
<li id="menu-item-21" class="megamenu submenu_4col menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children"><a href="#">Elements<span class="subtitle"></span></a>
<ul class="sub-menu">
	<li id="menu-item-921" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children"><a href="#">Column 1<span class="subtitle"></span></a>
	<ul class="sub-menu">
		<li id="menu-item-925" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="accordion/index.html"><i class="fa fa-align-justify"></i>Accordion</a></li>
		<li id="menu-item-926" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="action-box/index.html"><i class="fa fa-bullhorn"></i>Action Box</a></li>
		<li id="menu-item-927" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="animation/index.html"><i class="fa fa-spinner fa-spin"></i>Animation</a></li>
		<li id="menu-item-928" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="button/index.html"><i class="fa fa-hand-o-up"></i>Button</a></li>
		<li id="menu-item-929" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="clients-carousel/index.html"><i class="fa fa-ellipsis-h"></i>Clients Carousel</a></li>
		<li id="menu-item-930" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="countdown-timer/index.html"><i class="fa fa-clock-o"></i>Countdown Timer</a></li>
	</ul>
</li>
	<li id="menu-item-922" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children"><a href="#">Column 2<span class="subtitle"></span></a>
	<ul class="sub-menu">
		<li id="menu-item-931" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="google-fonts/index.html"><i class="fa fa-font"></i>Google Fonts</a></li>
		<li id="menu-item-932" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="google-map/index.html"><i class="fa fa-map-marker"></i>Google Map</a></li>
		<li id="menu-item-933" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="members/index.html"><i class="fa fa-group"></i>Members</a></li>
		<li id="menu-item-934" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="message-box/index.html"><i class="fa fa-warning"></i>Message Box</a></li>
		<li id="menu-item-935" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="news-ticker/index.html"><i class="fa fa-terminal"></i>News Ticker</a></li>
		<li id="menu-item-937" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="pricing-table/index.html"><i class="fa fa-usd"></i>Pricing Table</a></li>
	</ul>
</li>
	<li id="menu-item-923" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children"><a href="#">Column 3<span class="subtitle"></span></a>
	<ul class="sub-menu">
		<li id="menu-item-936" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="portfolio-carousel/index.html"><i class="fa fa-image"></i>Portfolio + Carousel</a></li>
		<li id="menu-item-938" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="charts-bars/index.html"><i class="fa fa-tasks"></i>Charts &#038; Bars</a></li>
		<li id="menu-item-939" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="recent-posts-carousel/index.html"><i class="fa fa-list-alt"></i>Posts + Carousel</a></li>
		<li id="menu-item-940" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="service-box/index.html"><i class="fa fa-globe"></i>Service Box</a></li>
		<li id="menu-item-941" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="social-icons/index.html"><i class="fa fa-facebook-square"></i>Social Icons</a></li>
		<li id="menu-item-1463" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="process-box/index.html"><i class="fa fa-list-ul"></i>Process Box</a></li>
	</ul>
</li>
	<li id="menu-item-924" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children"><a href="#">Column 4<span class="subtitle"></span></a>
	<ul class="sub-menu">
		<li id="menu-item-944" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="tabs/index.html"><i class="fa fa-th-list"></i>Tabs</a></li>
		<li id="menu-item-943" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="table/index.html"><i class="fa fa-table"></i>Table</a></li>
		<li id="menu-item-945" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="testimonials/index.html"><i class="fa fa-quote-left"></i>Testimonials</a></li>
		<li id="menu-item-946" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="toggle/index.html"><i class="fa fa-chevron-down"></i>Toggle</a></li>
		<li id="menu-item-947" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="typography/index.html"><i class="fa fa-font"></i>Typography</a></li>
		<li id="menu-item-948" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="video/index.html"><i class="fa fa-youtube-play"></i>Video</a></li>
	</ul>
</li>
</ul>
</li>
<li id="menu-item-967" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children"><a href="shop/index.html">Shop<span class="subtitle"></span></a>
<ul class="sub-menu">
	<li id="menu-item-966" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="cart/index.html">Cart<span class="subtitle"></span></a></li>
	<li id="menu-item-965" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="checkout/index.html">Checkout<span class="subtitle"></span></a></li>
	<li id="menu-item-964" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="my-account/index.html">My Account<span class="subtitle"></span></a></li>
</ul>
</li>
<li id="menu-item-19" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children"><a href="contact-us/index.html">Contact Us<span class="subtitle"></span></a>
<ul class="sub-menu">
	<li id="menu-item-1447" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="contact-us-2-2/index.html">Contact Us 2<span class="subtitle"></span></a></li>
</ul>
</li>
</ul></nav>             
        </div><!--/row-->
    </div><!--/thead-->
    
    
</header><!-- end header -->
        			<!-- SLIDER -->   
            <div class="revwrap">
            
				<link href="http://fonts.googleapis.com/css?family=Ubuntu%3A700%2C300" rel="stylesheet" property="stylesheet" type="text/css" media="all" /><link href="http://fonts.googleapis.com/css?family=Roboto%3A500" rel="stylesheet" property="stylesheet" type="text/css" media="all" /><link href="http://fonts.googleapis.com/css?family=Open+Sans%3A300" rel="stylesheet" property="stylesheet" type="text/css" media="all" />


            </div>
            <!-- End SLIDER --> 
            
            
        

		<div class="page-content">
           <div class="row clearfix">
           
  					                    
                  	<div class="vc_row wpb_row vc_row-fluid vc_custom_1473104552943" id="firstLoader"><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="services services_b style1 services_padding  clearfix"  style="background:#ffffff;border:solid 1px #dddddd;"><i class="main fa fa-check-circle"  style="color:#099aad;" ></i><div><h3>Best Performance</h3><span class="bar"></span><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi.</p></div><a class='btn mrdd  tbutton5 customcolor' href='#' target='_self'  style="background-color:#099aad;color:#ffffff;" >Learn More</a></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="services services_b style1 services_padding  clearfix"  style="background:#ffffff;border:solid 1px #dddddd;"><i class="main fa fa-gears"  style="color:#099aad;" ></i><div><h3>Fully Customizable</h3><span class="bar"></span><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi.</p></div><a class='btn mrdd  tbutton5 customcolor' href='#' target='_self'  style="background-color:#099aad;color:#ffffff;" >Learn More</a></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="services services_b style1 services_padding  clearfix"  style="background:#ffffff;border:solid 1px #dddddd;"><i class="main fa fa-globe"  style="color:#099aad;" ></i><div><h3>Global Design</h3><span class="bar"></span><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi.</p></div><a class='btn mrdd  tbutton5 customcolor' href='#' target='_self'  style="background-color:#099aad;color:#ffffff;" >Learn More</a></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="services services_b style1 services_padding  clearfix"  style="background:#ffffff;border:solid 1px #dddddd;"><i class="main fa fa-rocket"  style="color:#099aad;" ></i><div><h3>Ready To Launch</h3><span class="bar"></span><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi.</p></div><a class='btn mrdd  tbutton5 customcolor' href='#' target='_self'  style="background-color:#099aad;color:#ffffff;" >Learn More</a></div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gap clearfix" style="height: 60px;"></div>
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h2 style="text-align: center;">Totally New Business Way</h2>
<h4 style="text-align: center;">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</h4>

		</div>
	</div>
<link href="http://fonts.googleapis.com/css?family=Monda" rel="stylesheet" type="text/css">
      			<div class="gfont clearfix" style="font-family:'Monda', serif !important; font-size:50px !important; line-height:50px !important;  color:rgba(9,154,173,0.3);  text-align:center;  margin: 0px !important;">▼</div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gap clearfix" style="height: 40px;"></div><div class="services style1 right clearfix" ><i class="main fa fa-gear" ></i><div><h3>Unlimited Options</h3><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem ut perspiciatis unde</p></div></div><div class="gap clearfix" style="height: 65px;"></div><div class="services style1 right clearfix" ><i class="main fa fa-arrows" ></i><div><h3>Drag and Drop</h3><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem ut perspiciatis unde</p></div></div><div class="gap clearfix" style="height: 65px;"></div><div class="services style1 right clearfix" ><i class="main fa fa-image" ></i><div><h3>Retina Graphics</h3><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem ut perspiciatis unde</p></div></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gap clearfix" style="height: 90px;"></div>
	<div class="wpb_single_image wpb_content_element vc_align_center">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="423" height="412" src="wp-content/uploads/2016/08/services1.png" class="vc_single_image-img attachment-full" alt="services1" srcset="http://demo.themetor.com/bizpro/wp-content/uploads/2016/08/services1.png 423w, http://demo.themetor.com/bizpro/wp-content/uploads/2016/08/services1-420x409.png 420w" sizes="(max-width: 423px) 100vw, 423px" /></div>
		</figure>
	</div>
<div class="gap clearfix" style="height: 80px;"></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gap clearfix" style="height: 40px;"></div><div class="services style1 left clearfix" ><i class="main fa fa-globe" ></i><div><h3>100% Translatable</h3><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem ut perspiciatis unde</p></div></div><div class="gap clearfix" style="height: 65px;"></div><div class="services style1 left clearfix" ><i class="main fa fa-support" ></i><div><h3>Free Support 24/7</h3><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem ut perspiciatis unde</p></div></div><div class="gap clearfix" style="height: 65px;"></div><div class="services style1 left clearfix" ><i class="main fa fa-bars" ></i><div><h3>Parallax Sections</h3><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem ut perspiciatis unde</p></div></div></div></div></div></div><div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid tar vc_custom_1473022964624 vc_row-has-fill"><div class="wpb_column vc_column_container vc_col-sm-8"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h2 style="text-align: left;"><span style="color: #ffffff;">Do you have any question about our services?</span></h2>

		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper"><div class='text-right'><a class='btn mrdd large default customcolor' href='#' target='_self'  style="border-color:#ffffff;color:#ffffff;;" ><i class="mrd fa fa-phone-square"  style="color:#ffffff;"  ></i> Free Consultation</a></div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gap clearfix" style="height: 30px;"></div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-2"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="clearfix main-title title-left title-with-icon"><h2 class="title_line"><i class="title-icon fa fa-th" ></i>Latest Projects</h2></div><div class="gap clearfix" style="height: 50px;"></div><a class='btn mrdd small tbutton5 customcolor' href='#' target='_self'  style="background-color:#099aad;color:#ffffff;" ><i class="mrd fa fa-circle"  style="color:rgba(255,255,255,0.5);"  ></i> View All Projects</a></div></div></div><div class="wpb_column vc_column_container vc_col-sm-10"><div class="vc_column-inner "><div class="wpb_wrapper">                <div class="fit_carousel_portfolio owl-carousel  grid nav_side" data-speed="1000" data-pause="4000" data-col="3" id="pc_1473043995">
                	
				              	
			

					 
                    <figure class="item">
                        <a href="project/urban-management/index.html">
                            <img width="420" height="270" src="wp-content/uploads/2016/08/6-420x270.jpg" class="attachment-bizpro_blog1c size-bizpro_blog1c wp-post-image" alt="6" />                            <div class="figcaption">
                                <h2>Urban Management</h2>
                                <p>
                                Building, Consulting, Urban                                </p>
                            </div>
                        </a>
                    </figure>
					
				
				              	
			

					 
                    <figure class="item">
                        <a href="project/business-finance/index.html">
                            <img width="420" height="270" src="wp-content/uploads/2016/08/8-420x270.jpg" class="attachment-bizpro_blog1c size-bizpro_blog1c wp-post-image" alt="8" />                            <div class="figcaption">
                                <h2>Business Finance</h2>
                                <p>
                                Finance, Industrial                                </p>
                            </div>
                        </a>
                    </figure>
					
				
				              	
			

					 
                    <figure class="item">
                        <a href="project/project-title/index.html">
                            <img width="420" height="270" src="wp-content/uploads/2016/08/4-420x270.jpg" class="attachment-bizpro_blog1c size-bizpro_blog1c wp-post-image" alt="4" />                            <div class="figcaption">
                                <h2>Project Title</h2>
                                <p>
                                Building, Urban                                </p>
                            </div>
                        </a>
                    </figure>
					
				
				              	
			

					 
                    <figure class="item">
                        <a href="project/manufacturing-process-management/index.html">
                            <img width="420" height="270" src="wp-content/uploads/2016/08/9-420x270.jpg" class="attachment-bizpro_blog1c size-bizpro_blog1c wp-post-image" alt="9" />                            <div class="figcaption">
                                <h2>Manufacturing Process Management</h2>
                                <p>
                                Consulting, Finance, Industrial                                </p>
                            </div>
                        </a>
                    </figure>
					
				
				              	
			

					 
                    <figure class="item">
                        <a href="project/building-structure/index.html">
                            <img width="420" height="270" src="wp-content/uploads/2016/09/2-1-420x270.jpg" class="attachment-bizpro_blog1c size-bizpro_blog1c wp-post-image" alt="2" />                            <div class="figcaption">
                                <h2>Building Structure</h2>
                                <p>
                                Building, Urban                                </p>
                            </div>
                        </a>
                    </figure>
					
				
				              	
			

					 
                    <figure class="item">
                        <a href="project/industrial-analysis/index.html">
                            <img width="420" height="270" src="wp-content/uploads/2016/08/1-420x270.jpg" class="attachment-bizpro_blog1c size-bizpro_blog1c wp-post-image" alt="1" />                            <div class="figcaption">
                                <h2>Industrial Analysis</h2>
                                <p>
                                Consulting, Industrial                                </p>
                            </div>
                        </a>
                    </figure>
					
					
			</div>
				
				
				
            

 
 </div></div></div></div><div class="gap clearfix" style="height: 30px;"></div><div class="gap clearfix" style="height: 70px;"></div><div class="clearfix main-title title-center"><h2 class="title_line">How we work?</h2></div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="process_box vertical_process"><span style="color:rgba(9,154,173,0.15)">1</span><h3>Free Consultation</h3><p>Lorem ipsum consectetuer adipiscing elit. Nam cursus. Morbi ut mi.</p></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="process_box vertical_process"><span style="color:rgba(9,154,173,0.35)">2</span><h3>Research </h3><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus.</p></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="process_box vertical_process"><span style="color:rgba(9,154,173,0.68)">3</span><h3>Contract &amp; Payment</h3><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit mi.</p></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="process_box vertical_process"><span style="color:#099aad">4</span><h3>Project Done!</h3><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi ut mi.</p></div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gap clearfix" style="height: 40px;"></div><div class='text-center'><a class='btn mrdd medium default customcolor' href='#' target='_self'  style="border-color:rgba(9,154,173,0.4);color:#444444;;" ><i class="mrd fa fa-phone"  style="color:#099aad;"  ></i> Get Free Consultation</a></div><div class="gap clearfix" style="height: 60px;"></div></div></div></div></div><div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-parallax="2.5" data-vc-parallax-image="http://demo.themetor.com/bizpro/wp-content/uploads/2016/08/para6.jpg" class="vc_row wpb_row vc_row-fluid tar para-border vc_custom_1473170988004 vc_row-has-fill vc_general vc_parallax vc_parallax-content-moving"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gap clearfix" style="height: 30px;"></div><div class="clearfix main-title title-center"><h2 class="title_line">What our customers said?</h2></div>		
        <!-- testimonial -->
		<div class="bizpro_testimonials light"  style="background:#099aad;" >
			<div id="tst_132073894" class="custom_block2">
				<div class="inner_list">
					<ul><li><a href="#tst_1" ><img width="100" height="100" src="wp-content/uploads/2016/08/tst3.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="tst3" /></a></li><li><a href="#tst_2" ><img width="100" height="100" src="wp-content/uploads/2016/08/tst2.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="tst2" /></a></li><li><a href="#tst_3" ><img width="100" height="100" src="wp-content/uploads/2016/08/tst1.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="tst1" /></a></li></ul>
				</div>
			</div>
			
			<div id="tst_252708474" class="custom_block2_testimonial light"><div class="h_slider"><span class="tst_1">&quot;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown&quot;<small> - John Smith (Designer)</small></span></div><div class="h_slider"><span class="tst_2">&quot;Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In posuere felis nec tortor. Pellentesque faucibus.&quot;<small> - Jennifer Raya  </small></span></div><div class="h_slider"><span class="tst_3">&quot;Nunc tempus felis vitae urna. Vivamus porttitor, neque at volutpat rutrum, purus nisi eleifend libero, a tempus libero lectus feugiat felis. Morbi diam mauris, viverra in, gravida eu, mattis in ante.&quot;<small> - Nicolas Cool (Manager)</small></span></div></div>
		</div>
		<!-- /testimonial -->

<div class="gap clearfix" style="height: 50px;"></div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gap clearfix" style="height: 20px;"></div>
<div class="vc_chart vc_line-chart wpb_content_element" data-vc-legend="1" data-vc-tooltips="1" data-vc-animation="easeinOutCubic" data-vc-type="line" data-vc-values="{&quot;labels&quot;:[&quot;JAN&quot;,&quot; FEB&quot;,&quot; MAR&quot;,&quot; APR&quot;,&quot; MAY&quot;,&quot; JUN&quot;,&quot; JUL&quot;,&quot; AUG&quot;,&quot; SEP&quot;,&quot; OCT&quot;,&quot;NOV&quot;],&quot;datasets&quot;:[{&quot;label&quot;:&quot;Your Item 1&quot;,&quot;fillColor&quot;:&quot;rgba(235, 235, 235, 0.1)&quot;,&quot;strokeColor&quot;:&quot;#ebebeb&quot;,&quot;pointColor&quot;:&quot;#ebebeb&quot;,&quot;pointStrokeColor&quot;:&quot;#ebebeb&quot;,&quot;highlightFill&quot;:&quot;#dcdcdc&quot;,&quot;highlightStroke&quot;:&quot;#dcdcdc&quot;,&quot;pointHighlightFill&quot;:&quot;#dcdcdc&quot;,&quot;pointHighlightStroke&quot;:&quot;#dcdcdc&quot;,&quot;data&quot;:[&quot;10&quot;,&quot; 15&quot;,&quot; 20&quot;,&quot; 25&quot;,&quot; 27&quot;,&quot; 25&quot;,&quot; 23&quot;,&quot; 25&quot;,&quot;27&quot;,&quot;24&quot;,&quot;20&quot;]},{&quot;label&quot;:&quot;Item Two&quot;,&quot;fillColor&quot;:&quot;rgba(76, 173, 201, 0.1)&quot;,&quot;strokeColor&quot;:&quot;#4cadc9&quot;,&quot;pointColor&quot;:&quot;#4cadc9&quot;,&quot;pointStrokeColor&quot;:&quot;#4cadc9&quot;,&quot;highlightFill&quot;:&quot;#39a0bd&quot;,&quot;highlightStroke&quot;:&quot;#39a0bd&quot;,&quot;pointHighlightFill&quot;:&quot;#39a0bd&quot;,&quot;pointHighlightStroke&quot;:&quot;#39a0bd&quot;,&quot;data&quot;:[&quot;25&quot;,&quot; 18&quot;,&quot; 16&quot;,&quot; 17&quot;,&quot; 20&quot;,&quot; 25&quot;,&quot; 27&quot;,&quot;25&quot;,&quot;29&quot;,&quot;30&quot;,&quot;28&quot;]}]}">
	
	<div class="wpb_wrapper">
		<div class="vc_chart-with-legend"><canvas class="vc_line-chart-canvas" width="1" height="1"></canvas></div><ul class="vc_chart-legend"><li><span style="background-color:#ebebeb"></span>Your Item 1</li><li><span style="background-color:#4cadc9"></span>Item Two</li></ul>
	</div>
</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="clearfix main-title title-center"><h3 class="title_line">Our Skills</h3></div><div class="gap clearfix" style="height: 20px;"></div><div class="progress-bar style-animation">
			<span data-rel="88" style="background-color:#099aad !important;"><b style="background-color:#099aad !important;color:#099aad !important;"><cite>88%</cite></b></span>
			<div class="progress-bar-text">Management</div>
			</div><div class="progress-bar style-animation">
			<span data-rel="47" style="background-color:#099aad !important;"><b style="background-color:#099aad !important;color:#099aad !important;"><cite>47%</cite></b></span>
			<div class="progress-bar-text">Your Title</div>
			</div><div class="progress-bar style-animation">
			<span data-rel="66" style="background-color:#099aad !important;"><b style="background-color:#099aad !important;color:#099aad !important;"><cite>66%</cite></b></span>
			<div class="progress-bar-text">CRM</div>
			</div><div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class= "vc_pie_chart wpb_content_element" data-pie-value="73" data-pie-label-value="" data-pie-units="" data-pie-color="#099aad"><div class="wpb_wrapper"><div class="vc_pie_wrapper"><span class="vc_pie_chart_back" style="border-color: #099aad"></span><span class="vc_pie_chart_value"></span><canvas width="101" height="101"></canvas></div><h4 class="wpb_heading wpb_pie_chart_heading">Management</h4></div></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class= "vc_pie_chart wpb_content_element" data-pie-value="35" data-pie-label-value="$35/hr" data-pie-units="" data-pie-color="#099aad"><div class="wpb_wrapper"><div class="vc_pie_wrapper"><span class="vc_pie_chart_back" style="border-color: #099aad"></span><span class="vc_pie_chart_value"></span><canvas width="101" height="101"></canvas></div><h4 class="wpb_heading wpb_pie_chart_heading">WordPress</h4></div></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class= "vc_pie_chart wpb_content_element" data-pie-value="68" data-pie-label-value="68k" data-pie-units="" data-pie-color="#099aad"><div class="wpb_wrapper"><div class="vc_pie_wrapper"><span class="vc_pie_chart_back" style="border-color: #099aad"></span><span class="vc_pie_chart_value"></span><canvas width="101" height="101"></canvas></div><h4 class="wpb_heading wpb_pie_chart_heading">Likes</h4></div></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class= "vc_pie_chart wpb_content_element" data-pie-value="87" data-pie-label-value="87%" data-pie-units="" data-pie-color="#099aad"><div class="wpb_wrapper"><div class="vc_pie_wrapper"><span class="vc_pie_chart_back" style="border-color: #099aad"></span><span class="vc_pie_chart_value"></span><canvas width="101" height="101"></canvas></div><h4 class="wpb_heading wpb_pie_chart_heading">CRM</h4></div></div></div></div></div></div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gap clearfix" style="height: 50px;"></div></div></div></div></div><div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-parallax="2.5" data-vc-parallax-image="http://demo.themetor.com/bizpro/wp-content/uploads/2016/08/slide8.jpg" class="vc_row wpb_row vc_row-fluid tac para-border vc_row-has-fill vc_general vc_parallax vc_parallax-content-moving"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gap clearfix" style="height: 70px;"></div>
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h2>Do you have any business problem?</h2>
<h3>We are your solution!</h3>

		</div>
	</div>
<div class='text-center'><a class='btn mrdd medium tbutton5 customcolor' href='#' target='_self'  style="background-color:#099aad;color:#ffffff;" ><i class="mrd fa fa-file-o"  style="color:#ffffff;"  ></i> Lean more...</a></div><div class="gap clearfix" style="height: 100px;"></div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gap clearfix" style="height: 20px;"></div></div></div></div></div><div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1474329284117 vc_row-has-fill vc_row-o-equal-height vc_row-flex"><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_left">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="550" height="680" src="wp-content/uploads/2016/08/package.jpg" class="vc_single_image-img attachment-full" alt="package" srcset="http://demo.themetor.com/bizpro/wp-content/uploads/2016/08/package.jpg 550w, http://demo.themetor.com/bizpro/wp-content/uploads/2016/08/package-420x519.jpg 420w" sizes="(max-width: 550px) 100vw, 550px" /></div>
		</figure>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-8"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gap clearfix" style="height: 70px;"></div><div class="clearfix main-title title-left title-with-icon"><h2 class="title_line"><i class="title-icon fa fa-info-circle" ></i>Everyboy has something important to say!</h2></div><hr class="line" style="background-color:#dddddd;float:left;margin-top:0;" ><div class="clearfix"></div>
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p style="text-align: justify;"><span style="color: #808080;">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</span></p>

		</div>
	</div>
<a class='btn mrdd medium default default' href='#' target='_self'  style="color:#444444;;" ><i class="mrd fa fa-caret-right"  style="color:#444444;"  ></i> Free Consultation</a><a class='btn mrdd medium tbutton5 customcolor' href='#' target='_self'  style="background-color:#099aad;color:#ffffff;" ><i class="mrd fa fa-shopping-cart"  style="color:#ffffff;"  ></i> Buy it Now</a><div class="gap clearfix" style="height: 100px;"></div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="divider "><hr class="icon" style="margin-top:0;" ><i class="fa fa-angle-down" ></i></div><div class="gap clearfix" style="height: 50px;"></div>
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h2 style="text-align: center;">Our Clients</h2>
<h4 style="text-align: center;">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</h4>

		</div>
	</div>
	
	    	
            <div class="clients">

              <ul id="cc_1748849044" data-speed="4000" data-pause="1000" data-col="6" class="logo_carousel owl-carousel nav_side">
                				 		
					                    <li><a href="http://virqo.com/" title="virqo.com" class="toptip" target="_blank" >     

                                                        
                            <img width="248" height="120" src="wp-content/uploads/2016/09/8r.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="8r" />                                    
                        
                    </a></li>
                    
                				 		
					                    <li><a href="http://elmim.com/" title="elmim.com" class="toptip" target="_blank" >     

                                                        
                            <img width="248" height="120" src="wp-content/uploads/2016/09/7r.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="7r" />                                    
                        
                    </a></li>
                    
                				 		
					                    <li><a href="http://omlia.com/" title="omlia.com" class="toptip" target="_blank" >     

                                                        
                            <img width="248" height="120" src="wp-content/uploads/2016/09/4r.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="4r" />                                    
                        
                    </a></li>
                    
                				 		
					                    <li><a href="http://yooxle.com/" title="yooxle.com" class="toptip" target="_blank" >     

                                                        
                            <img width="248" height="120" src="wp-content/uploads/2016/09/5r.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="5r" />                                    
                        
                    </a></li>
                    
                				 		
					                    <li><a href="http://netzif.com/" title="netzif.com" class="toptip" target="_blank" >     

                                                        
                            <img width="248" height="120" src="wp-content/uploads/2016/09/6r.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="6r" />                                    
                        
                    </a></li>
                    
                				 		
					                    <li><a href="http://zitix.com/" title="zitix.com" class="toptip" target="_blank" >     

                                                        
                            <img width="248" height="120" src="wp-content/uploads/2016/09/1r.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="1r" />                                    
                        
                    </a></li>
                    
                              </ul>
           </div>

     </div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gap clearfix" style="height: 50px;"></div></div></div></div></div><div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-parallax="2.5" data-vc-parallax-image="http://demo.themetor.com/bizpro/wp-content/uploads/2016/08/para6.jpg" class="vc_row wpb_row vc_row-fluid tac para-border vc_row-has-fill vc_general vc_parallax vc_parallax-content-moving"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gap clearfix" style="height: 70px;"></div>
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h1 style="text-align: center;"><span style="color: #444444;">Do you want new modern website?</span></h1>
<h3 style="text-align: center;"><span style="color: #444444;">Yes, of course! So let&#8217;s buy BizPro now</span></h3>

		</div>
	</div>
<div class='text-center'><a class='btn mrdd medium tbutton5 customcolor' href='#' target='_self'  style="background-color:#099aad;color:#ffffff;" ><i class="mrd fa fa-arrow-down"  style="color:#ffffff;"  ></i> Buy BizPro Now!</a></div><div class="gap clearfix" style="height: 100px;"></div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gap clearfix" style="height: 50px;"></div>
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h2 style="text-align: center;">Pricing</h2>
<h4 style="text-align: center;">This pricing table is for demo purpose and not actual services price</h4>

		</div>
	</div>
<div class="divider "><hr class="icon" style="width:25%;margin-top:0;" ><i class="fa fa-angle-down" ></i></div><div class="gap clearfix" style="height: 50px;"></div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="pricing-table"><div class="head"><h4>Starter</h4><h2>$1.99</h2><span>Daily</span></div><div class="price-content"><img class="alignnone size-full wp-image-516" src="wp-content/uploads/2016/08/package1-1.jpg" alt="package1" width="400" height="400" srcset="http://demo.themetor.com/bizpro/wp-content/uploads/2016/08/package1-1.jpg 400w, http://demo.themetor.com/bizpro/wp-content/uploads/2016/08/package1-1-150x150.jpg 150w" sizes="(max-width: 400px) 100vw, 400px" /></p>
<ul>
<li>Option #1</li>
<li>Second Option<strong><br />
</strong></li>
<li><em>Important Option!</em></li>
<li><del>Best Feature</del><del></del></li>
</ul>
<div class="clearfix mtt"><a class='btn mrdd small  ' href='#' target='_self'  style=";" >Order Now</a></div></div></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="pricing-table featured_table"><div class="recommended"  style="background:#099aad;color:;">Best Seller</div><div class="head"><h4>Premium</h4><h2>$29.99</h2><span>Monthly</span></div><div class="price-content"><img class="alignnone size-full wp-image-516" src="wp-content/uploads/2016/08/package2-1.jpg" alt="package2" width="400" height="400" /></p>
<ul>
<li>Option #1</li>
<li>Second Option<strong><br />
</strong></li>
<li><em>Important Option!</em></li>
<li>Best Feature</li>
<li><strong>Bold Feature</strong></li>
</ul>
<div class="clearfix mtt"><a class='btn mrdd small tbutton5 customcolor' href='#' target='_self'  style="background-color:#099aad;color:#ffffff;" ><i class="mrd fa fa-thumbs-up"  style="color:#ffffff;"  ></i> Order Now</a></div></div></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="pricing-table"><div class="head"><h4>Team Plan</h4><h2>$49.99</h2><span>Monthly</span></div><div class="price-content"><img class="alignnone size-full wp-image-516" src="wp-content/uploads/2016/08/package3-1.jpg" alt="package3" width="400" height="400" /></p>
<ul>
<li>Option #1<strong><br />
</strong></li>
<li><em>Important Option!</em></li>
<li>Best Feature</li>
<li><strong>Bold Feature</strong></li>
</ul>
<div class="clearfix mtt"><a class='btn mrdd small  ' href='#' target='_self'  style=";" >Order Now</a></div></div></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="pricing-table"><div class="head"><h4>Professional</h4><h2>$99.99</h2><span>Yearly</span></div><div class="price-content"><img class="alignnone size-full wp-image-516" src="wp-content/uploads/2016/08/package4-1.jpg" alt="package4" width="400" height="400" /></p>
<ul>
<li>All Features<strong><br />
</strong></li>
<li><em>Important Option!</em></li>
<li>Best Feature</li>
<li><strong>Bold Feature</strong></li>
</ul>
<div class="clearfix mtt"><a class='btn mrdd small  ' href='#' target='_self'  style=";" >Order Now</a></div></div></div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gap clearfix" style="height: 70px;"></div>
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h2 style="text-align: center;">Any Question?</h2>

		</div>
	</div>
<div class='text-center'><a class='btn mrdd xlarge default customcolor' href='#' target='_self'  style="border-color:rgba(9,154,173,0.4);color:#099aad;;" ><i class="mrd fa fa-phone"  style="color:#099aad;"  ></i> Free Call +1 (800) 123456</a></div><div class="gap clearfix" style="height: 70px;"></div></div></div></div></div>
                                            
                                        
                    
                    
            </div><!-- end row -->    
		</div><!-- end page-content -->
        
        
    
<footer>
    <div class="footer_widgets row clearfix">
      <div id="text-2" class="widget footer_widget widget_text grid_3">			<div class="textwidget"><img alt="Biz Pro" src="wp-content/themes/bizpro/images/logo.png" width="160">
<br/>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis ege stas Morbi magna magna.

<div class="social social_widget w_radius w_border mbf clearfix"><a href="#" class="toptip ico-facebook" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a><a href="#" class="toptip ico-twitter" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a><a href="#" class="toptip ico-google-plus" title="Google_plus" target="_blank"><i class="fa fa-google-plus"></i></a><a href="#" class="toptip ico-linkedin" title="Linkedin" target="_blank"><i class="fa fa-linkedin"></i></a><a href="#" class="toptip ico-rss" title="Rss" target="_blank"><i class="fa fa-rss"></i></a></div></div>
		</div><div id="bizpro_portfolio-2" class="widget footer_widget widget_bizpro_portfolio grid_3"><h4>Latest Project</h4>			<div class="recent recent-portfolio"><ul>
				                                	<li>
                    
                                                <div class="post-thumbnail">
                            
                            <a class="mini-thumb toptip portfolio-pic" href="project/urban-management/index.html" title="Urban Management" >
								<img width="150" height="150" src="wp-content/uploads/2016/08/6-150x150.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="6" />                            </a>

                        </div>
						                    	
                        
                    	
                    
                    </li>
                                	<li>
                    
                                                <div class="post-thumbnail">
                            
                            <a class="mini-thumb toptip portfolio-pic" href="project/business-finance/index.html" title="Business Finance" >
								<img width="150" height="150" src="wp-content/uploads/2016/08/8-150x150.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="8" />                            </a>

                        </div>
						                    	
                        
                    	
                    
                    </li>
                                	<li>
                    
                                                <div class="post-thumbnail">
                            
                            <a class="mini-thumb toptip portfolio-pic" href="project/project-title/index.html" title="Project Title" >
								<img width="150" height="150" src="wp-content/uploads/2016/08/4-150x150.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="4" />                            </a>

                        </div>
						                    	
                        
                    	
                    
                    </li>
                                	<li>
                    
                                                <div class="post-thumbnail">
                            
                            <a class="mini-thumb toptip portfolio-pic" href="project/manufacturing-process-management/index.html" title="Manufacturing Process Management" >
								<img width="150" height="150" src="wp-content/uploads/2016/08/9-150x150.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="9" />                            </a>

                        </div>
						                    	
                        
                    	
                    
                    </li>
                                	<li>
                    
                                                <div class="post-thumbnail">
                            
                            <a class="mini-thumb toptip portfolio-pic" href="project/building-structure/index.html" title="Building Structure" >
								<img width="150" height="150" src="wp-content/uploads/2016/09/2-1-150x150.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="2" srcset="http://demo.themetor.com/bizpro/wp-content/uploads/2016/09/2-1-150x150.jpg 150w, http://demo.themetor.com/bizpro/wp-content/uploads/2016/09/2-1-180x180.jpg 180w, http://demo.themetor.com/bizpro/wp-content/uploads/2016/09/2-1-300x300.jpg 300w, http://demo.themetor.com/bizpro/wp-content/uploads/2016/09/2-1-600x600.jpg 600w" sizes="(max-width: 150px) 100vw, 150px" />                            </a>

                        </div>
						                    	
                        
                    	
                    
                    </li>
                                	<li>
                    
                                                <div class="post-thumbnail">
                            
                            <a class="mini-thumb toptip portfolio-pic" href="project/industrial-analysis/index.html" title="Industrial Analysis" >
								<img width="150" height="150" src="wp-content/uploads/2016/08/1-150x150.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="1" />                            </a>

                        </div>
						                    	
                        
                    	
                    
                    </li>
                			</ul></div>
            
            
	
			</div><div id="bizpro_contact-2" class="widget footer_widget widget_bizpro_contact grid_3"><h4>Contact Us</h4><div class="address">            	<p> No. 4, 23rd Alley, This Ave. City, Country </p>
			            	<div> <i class="fa fa-phone"  ></i>+1(800)123456 &nbsp;&nbsp;|&nbsp;&nbsp;+1(800)123457 </div>
			            	<div> <i class="fa fa-print"  ></i>+1800123456 </div>
			            	<div><a href="mailto:email@yourwebsite.com" target="_blank"><i class="fa fa-envelope-o"  ></i>email@yourwebsite.com</a> </div>
			            	<div><a href="http://maps.google.com/" target="_blank"><i class="fa fa-map-marker"  ></i>See Map</a> </div>
			</div></div><div id="search-4" class="widget footer_widget widget_search grid_3"><h4>Search</h4><form action="http://demo.themetor.com/bizpro/" id="searchwidget" method="get">
    <input name="s" type="text" value="" placeholder="Search...">
    <button type="submit"><i class="fa fa-search"></i></button>
</form></div><div id="bizpro_subscription-2" class="widget footer_widget widget_bizpro_subscription grid_3"><h4>Join Us</h4>            
            <form method="post" id="newsletter" action="http://feedburner.google.com/fb/a/mailverify" target="popupwindow" onsubmit="window.open('http://feedburner.google.com/fb/a/mailverify', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">
                <input type="email" name="email" class="inputer" placeholder="Type Your Email" required>
                <button type="submit"><i class="fa fa-plus"></i></button>
            </form>
            
            </div>    </div><!--/row-->
    
    <div class="sub_footer">
        <div class="row clearfix both-center">
            <div class="menu-footer-menu-container"><ul id="menu-footer-menu" class="foot-menu"><li id="menu-item-773" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4 current_page_item menu-item-773"><a href="index.html">Homepage</a></li>
<li id="menu-item-774" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-774"><a href="about-us/index.html">About Us</a></li>
<li id="menu-item-776" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-776"><a href="faq/index.html">FAQ</a></li>
<li id="menu-item-777" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-777"><a href="#">Privacy &#038; Policy</a></li>
<li id="menu-item-778" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-778"><a href="#">Terms &#038; Conditions</a></li>
<li id="menu-item-775" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-775"><a href="contact-us/index.html">Contact Us</a></li>
</ul></div>                <span class="copyright">Copyright © 2016 BizPro Theme. Designed by <a href="http://themeforest.net/user/themetor?ref=themetor" target="_blank">ThemeTor</a>.</span>
				    
        </div><!--/row-->
    </div><!--/sub_footer-->
</footer>

<div id="toTop"><i class="fa fa-angle-up"></i></div><!--/totop icon-->

</div><!-- end layout -->


		<script type="text/javascript">
			function revslider_showDoubleJqueryError(sliderID) {
				var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
				errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
				errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
				errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
				errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
					jQuery(sliderID).show().html(errorMessage);
			}
		</script>
		<link rel='stylesheet' id='customizercss_css'  href='wp-content/themes/bizpro/customizer/style5152.css?ver=1.0' type='text/css' media='all' />
<script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/jquery.form.mind03d.js?ver=3.51.0-2014.06.20'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"http:\/\/demo.themetor.com\/bizpro\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}},"sending":"Sending ...","cached":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts8686.js?ver=4.5.1'></script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min44fd.js?ver=2.70'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/bizpro\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/bizpro\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min72e6.js?ver=2.6.4'></script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/jquery-cookie/jquery.cookie.min330a.js?ver=1.4.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/bizpro\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/bizpro\/?wc-ajax=%%endpoint%%","fragment_name":"wc_fragments"};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min72e6.js?ver=2.6.4'></script>
<script type='text/javascript' src='wp-includes/js/wp-embed.mina07c.js?ver=4.6.11'></script>
<script type='text/javascript' src='wp-content/themes/bizpro/customizer/colorpicker4963.js?ver=1.1'></script>
<script type='text/javascript' src='wp-content/themes/bizpro/customizer/script62ea.js?ver=1.2'></script>
<script type='text/javascript' src='wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min9d08.js?ver=4.12.1'></script>
<script type='text/javascript' src='wp-content/plugins/js_composer/assets/lib/bower/skrollr/dist/skrollr.min9d08.js?ver=4.12.1'></script>
<script type='text/javascript' src='wp-content/plugins/js_composer/assets/lib/waypoints/waypoints.min9d08.js?ver=4.12.1'></script>
<script type='text/javascript' src='wp-content/plugins/js_composer/assets/lib/bower/chartjs/Chart.min9d08.js?ver=4.12.1'></script>
<script type='text/javascript' src='wp-content/plugins/js_composer/assets/lib/vc_line_chart/vc_line_chart.min9d08.js?ver=4.12.1'></script>
<script type='text/javascript' src='wp-content/plugins/js_composer/assets/lib/bower/progress-circle/ProgressCircle.min9d08.js?ver=4.12.1'></script>
<script type='text/javascript' src='wp-content/plugins/js_composer/assets/lib/vc_chart/jquery.vc_chart.min9d08.js?ver=4.12.1'></script>
    </form>
</body>
</html>